# image_labelling_GoogleForm

## Easily create a google form to label images!

Google Forms is a nice way to collect responses from a lot of people on a list of questions.
However, if you have a lot of questions, it can be annoying to build the form manually.
Luckily Google provides Apps Scripts (in Javascript) to automate your workflow.

In this project, you can see how I used it to create a Google Form in which participants can categorize images.
Doing this manually would have a high chance of introducing erros, and it would be hard to iterate on it when you want to make changes.

The created form shows a series of image to the participants and underneath each image, the possible catogeries are displayed as a multiple choice question. 
You can checkout the script [here](https://script.google.com/d/1OhTQ9eFW_DlJRZIrBBC3HV2LZ9QxVrXgk45XhWtY_DjRs1UZzsqIIxCm/edit?usp=sharing).

## To replicate this for your own dataset of images:
* Have the images publically available somewhere. 
The script can add images that are publically available on the web and adds them to the form. 
You can put the images on a website, or do as I did and add them to the public repository on GitLab, GitHub, or a similar service. 
As far as I know, it is not possible to add the images from your own computer if you want to use a script.
* Copy and change the script to find your images. To make life easier, I renamed every image so that the image name can easily be made during a for loop.
I suggest you have your file name in a similar way, e.g., "sample_0001.jpg", "sample_0002.jpg"... Or find a way to get the image names in another way.
* Change the questions to whatever you need.

## Remarks
* A script can only run for 5 minutes max.
I had to reduce the size of the images to speed up the script (checkout [this tutorial for Windows](https://www.howtogeek.com/354015/how-to-resize-images-and-photos-in-windows/) on how to do it for a batch of images)
After that, it was still to slow, so I just created two forms that each have 50% of the images.

If you need other components in the form or want some inspiration you can find more about the Apps Script API [here](https://developers.google.com/apps-script/reference/forms/).


